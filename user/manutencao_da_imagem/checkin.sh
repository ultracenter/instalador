#!/bin/bash

if [ $# != 1 ]
then
	echo "use $0 imagem.iso"
	exit
fi

echo 	"Aguarde ..."

# Limpa
umount  ~/mnt 		2>/dev/null
rm 	-rf ~/mnt 	2>/dev/null

# Prepara origem
mkdir 	~/mnt 

# Monta imagem
mount 	-r -o loop $1 ~/mnt 

# Cria pasta temporária.
mkdir 	~/tmp 

# Copia conteúdo
cp 	-r ~/mnt/* ~/tmp

# Acrescente o arquivo cdboot à pasta temporária.
cp 	cdboot ~/tmp/boot

echo 	"Check-in completado."
echo 	"Os arquivos estão em ~/tmp"


