#!/bin/bash

if [ $# != 1 ]
then
	echo "use $0 imagem.iso"
	exit
fi

echo "Aguarde ..."

# Gera uma nova imagem ISO
genisoimage -b boot/cdboot -no-emul-boot -r -J -V `date "+%d-%m-%Y"` -o $1 ~/tmp > /dev/null

umount  ~/mnt
rmdir   ~/mnt
rm 	-rf ~/tmp

echo "Check-out completado."

ls -lah $1


