#!/bin/sh

TERM=xterm
export TERM

cp syslog.conf /src/mfsbsd/work/mfs/etc

touch /src/mfsbsd/work/mfs/var/log/console.log

echo "-mq" > /src/mfsbsd/work/mfs/boot.config
echo "-mq" > /src/mfsbsd/work/disk/boot.config

cp /boot/entropy  /src/mfsbsd/work/disk/boot

cp loader.conf  /src/mfsbsd/conf/loader.conf.sample

cp loader.conf  /src/mfsbsd/work/mfs/boot
cp splash.bmp   /src/mfsbsd/work/mfs/boot

cp ../instalacao/kernel/splash_bmp.ko 	/src/mfsbsd/work/mfs/boot/modules
cp ../instalacao/kernel/splash_bmp.ko 	/src/mfsbsd/work/mfs/boot/modules

cp gettytab 	/src/mfsbsd/work/mfs/etc
cp ttys 	/src/mfsbsd/work/mfs/etc
cp ttys         /src/mfsbsd/conf

cp loader.conf  /src/mfsbsd/work/disk/boot
cp splash.bmp   /src/mfsbsd/work/disk/boot

cp ../instalacao/kernel/splash_bmp.ko 	/src/mfsbsd/work/disk/boot/kernel
cp ../instalacao/kernel/splash_bmp.ko 	/src/mfsbsd/work/disk/boot/kernel

echo "rc_startmsgs=\"NO\"" >> /src/mfsbsd/work/mfs/etc/rc.conf

tmp="/tmp/"`uuid | cut -d - -f 1`

rc="/src/mfsbsd/work/mfs/etc/rc"

cat $rc | grep -v "exit 0" > $tmp
cat $tmp > $rc

echo "mkdir /cdrom" >> $rc
echo "/sbin/mount -t cd9660 /dev/cd0 /cdrom" >> $rc
echo "cd /cdrom" >> $rc
echo "./instalar" >> $rc
echo "exit 0" >> $rc

rc="/src/mfsbsd/work/mfs/etc/rc"

cat $rc | grep -v "menu-display" > $tmp
cp $tmp  $rc


cp -r ../instalacao /src/mfsbsd/work/disk
cp -r ../freebsd    /src/mfsbsd/work/disk
cp -r ../pkg        /src/mfsbsd/work/disk

cp -r ../autoboot/cdrom/instalar /src/mfsbsd/work/disk

cp -r ../../user/manutencao_da_imagem /src/mfsbsd/work/disk


