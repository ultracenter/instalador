#!/bin/sh

. ./titulo.sh

_ip()
{

	ip=""
	mascara=""
	gateway=""
	dns_primario=""
	dns_secundario=""

	x=17
	len=25

	exec 3>&1

	VALUES=$(dialog --ok-label "Confirmar" --cancel-label "Cancelar"\
		  --backtitle "$titulo" \
		  --title "Configuracoes de rede" \
		  --form "\nInformacoes IP" \
	14 48 0 \
		"Endereco IP"    	1 1	"$ip"  			1 $x $len 0 \
		"Mascara"    		2 1	"$mascara"  		2 $x $len 0 \
		"Gateway"     		3 1	"$gateway" 		3 $x $len 0 \
		"DNS primario"     	4 1	"$dns_primario" 	4 $x $len 0 \
		"DNS secundario"     	5 1	"$dns_secundario" 	5 $x $len 0 \
	2>&1 1>&3)

	exec 3>&-

	echo "$VALUES" > /tmp/form

	a=`cat /tmp/form | tr '\n' '^'`

	ip=`echo $a | cut -d '^' -f 1`
	mascara=`echo $a | cut -d '^' -f 2`
	gateway=`echo $a | cut -d '^' -f 3`
	dns_primario=`echo $a | cut -d '^' -f 4`
	dns_secundario=`echo $a | cut -d '^' -f 5`

	saida=/tmp/dialog

	echo -n > $saida

	echo "$ip"		>> $saida
	echo "$mascara"	>> $saida
	echo "$gateway"	>> $saida
	echo "$dns_primario"	>> $saida
	echo "$dns_secundario"	>> $saida



}

#_ip ; cat /tmp/dialog



