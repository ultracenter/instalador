#!/bin/sh

. ./titulo.sh

TITLE='Aguarde...'
MSG='$1'
INTERVALO=1       # intervalo de atualização da barra (segundos)
PORCENTO=0        # porcentagem inicial da barra

#................................................................

ORIGEM="${2%/}"
DESTINO="${3%/}"

die()     { echo "Erro: $*" ; exit 1 ; }
sizeof()  { du -s "$1" | cut -f1; }
running() { ps $1 | grep $1 >/dev/null; }

#................................................................

# tem somente dois parâmetros?
[ "$3" ] || die "Uso: $0 mensagem dir-origem dir-destino"

# a origem e o destino devem ser diretórios
[ -d "$ORIGEM"  ] || die "A origem '$ORIGEM' deve ser um diretório"
[ -d "$DESTINO" ] || die "O destino '$DESTINO' deve ser um diretório"

# mesmo dir?
[ "$ORIGEM" = "$DESTINO" ] &&
        die "A origem e o destino são o mesmo diretório"

# o diretório de destino está vazio?
DIR_DESTINO="$DESTINO/${ORIGEM##*/}"
[ -d "$DIR_DESTINO" ] && [ $(sizeof $DIR_DESTINO) -gt 4 ] &&
        die "O dir de destino '$DIR_DESTINO' deveria estar vazio"

#................................................................

# expansão das variáveis da mensagem
MSG=$(eval echo $MSG)

# total a copiar (em bytes)
TOTAL=$(sizeof $ORIGEM)

# início da cópia, em segundo plano
cp -r $ORIGEM $DESTINO &
CPPID=$!

# caso o usuário cancele, interrompe a cópia
trap "kill $CPPID" 2 15

#................................................................

# loop de checagem de status da cópia
(
        # enquanto o processo de cópia estiver rodando
        while running $CPPID; do
        
                # quanto já foi copiado?
                COPIADO=$(sizeof $DIR_DESTINO)
                
                # qual a porcentagem do total?
                PORCENTAGEM=$((COPIADO*100/TOTAL))
                
                # envia a porcentagem para o dialog
                echo $PORCENTAGEM
                
                # aguarda até a próxima checagem
                sleep $INTERVALO
        done    

        # cópia finalizada, mostra a porcentagem final
        echo 100
        
) | dialog --backtitle "$titulo" --title "$TITLE" --gauge "  $MSG" 7 40 0

