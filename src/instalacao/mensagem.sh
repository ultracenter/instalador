#!/bin/sh

. ./titulo.sh


_bemvindo()
{
	dialog --backtitle "$titulo" --msgbox '\n   Artistas da monitoracao\n\n        Seja bem-vindo' 10 35
}

_introducao()
{
	dialog --backtitle "$titulo" --msgbox '\n\n   As telas a seguir vao guia-lo no processo de instalacao' 9 65

}

_concluido()
{
	dialog --backtitle "$titulo" --msgbox '\n           Concluido\n\n  Remova a midia de instalacao  \n' 10 35
}

_cancelado()
{
	dialog --backtitle "$titulo" --msgbox '\n\n          Cancelado\n' 10 35
}

_preencha()
{
	dialog --backtitle "$titulo" --msgbox "\n  Preencha\n\n  $1\n" 9 35
}

#_concluido

