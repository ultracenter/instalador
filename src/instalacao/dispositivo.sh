#!/bin/sh

. ./titulo.sh

_dispositivo()
{
	_enumera_dispositivos()
	{
	    local a
	    for i in `/sbin/camcontrol devlist | tr ' ' '^'`;
	    do
		a=`echo $i | tr '^' ' '`
		b=`echo $a | cut -d '<' -f 2 | cut -d '>' -f 1`
		c=`echo $a | cut -d '(' -f 2 | cut -d ')' -f 1 | cut -d ',' -f 2`

		b1=`echo $b | tr ' ' '_'`
		c1=`echo $c | tr ' ' '_'`

	        echo $c1 $b1
	    done
	}

	i=`_enumera_dispositivos`

	 dialog             \
	   --backtitle "$titulo" \
	   --title 'Dispositivo de destino'  \
	   --menu ' '  \
	   0 0 0             \
	   $i 2> /tmp/dialog

}

#_dispositivo


