#!/bin/sh

TERM=xterm
export TERM

DIALOGRC=instalar.rc
export DIALOGRC

REBOOT=reboot
#REBOOT="echo reiniciando... ; exit"
export REBOOT

EXIT=exit
#EXIT="echo saindao... ; exit"
export EXIT

SHUTDOWN="shutdown -r now"
#SHUTDOWN="echo shutdown... ; exit"
export SHUTDOWN

SEM_INTRODUCAO=1
export SEM_INTRODUCAO

IP_AUTOMATICO=13
export IP_AUTOMATICO


. ./dispositivo.sh
. ./interface.sh
. ./hostname.sh
. ./ip.sh
. ./opcoes.sh
. ./mensagem.sh


_instalar()
{

	# nome do pool
	local pool="p`/bin/date +%s`"

	local dispositivo
        local hostname
        local interface
        local ip
        local mascara
        local gateway
	local dns_primario
	local dns_secundario
	local informacoes_basicas
	local ip_automatico

	local parm=$1
	local len
	len=${#parm}
	if [ $len -eq 0 ]
	then
		_bemvindo  
		_bemvindo  
	fi

	_introducao

	# Dispositivo de destino
	_dispositivo ; dispositivo=`cat /tmp/dialog` 
	if [ -z $dispositivo ]; 
	then 
		_cancelado 
		clear
		$SHUTDOWN
		$EXIT
	fi


	# Interface
	_interface ; interface=`cat /tmp/dialog` 
	if [ -z $interface ]; 
	then 
		_cancelado 
		clear
		$SHUTDOWN
		$EXIT 
	fi

	local a

	# hostname
	_hostname ; hostname=`cat /tmp/dialog` 
	if [ -z $hostname ]; 
	then 
		_preencha "Nome do host"
		$0 SEM_INTRODUCAO
		$EXIT 
	fi


	# opcoes
	_opcoes

	ip_automatico=`cat /tmp/dialog    | tr ' ' '\n' | grep -i ip_automatico`

	local n

	# IP
	len=${#ip_automatico}

	if [ $len -ne $IP_AUTOMATICO ]
	then
		_ip

		ip=`echo $a | cut -d '^' -f 1`
		mascara=`echo $a | cut -d '^' -f 2`
		gateway=`echo $a | cut -d '^' -f 3`
		dns_primario=`echo $a | cut -d '^' -f 4`
		dns_secundario=`echo $a | cut -d '^' -f 5`


		n=`echo -n "$ip" | wc -c`
		if [ $n == 0 ];
		then 
			_preencha "Endereco IP"
			$0 SEM_INTRODUCAO
			$EXIT 
		fi

		n=`echo -n "$mascara" | wc -c`
		if [ $n == 0 ];
		then 
			_preencha "Mascara"
			$0 SEM_INTRODUCAO
			$EXIT 
		fi

		n=`echo -n "$gateway" | wc -c`
		if [ $n == 0 ];
		then 
			_preencha "Gateway"
			$0 SEM_INTRODUCAO
			$EXIT 
		fi

		n=`echo -n "$dns_primario" | wc -c`
		if [ $n == 0 ];
		then 
			_preencha "DNS primario"
			$0 SEM_INTRODUCAO
			$EXIT 
		fi

		n=`echo -n "$dns_secundario" | wc -c`
		if [ $n == 0 ];
		then 
			_preencha "DNS secundario"
			$0 SEM_INTRODUCAO
			$EXIT 
		fi
	fi


	clear

	# Destroi o conteudo do dispositivo de destino
	
	# Inicializa dispositivo
	/bin/dd if=/dev/zero of=/dev/$dispositivo bs=32M count=1
	/root/bin/destroygeom -d $dispositivo 2> /dev/null
	/sbin/gpart destroy -F $dispositivo 

	# Instala sistema basico
	/root/bin/zfsinstall -d $dispositivo -u /cdrom/freebsd -s 1G -p $pool

	# Silencia o sendmail
	echo -n > /mnt/usr/sbin/sendmail

 	# Instala novo kernel
 	echo "Instalando kernel ..."
 	mv /mnt/boot/kernel /mnt/boot/kernel.old 2> /dev/null
 	#cp -r kernel /mnt/boot
	./copydir.sh "Instalando kernel" kernel /mnt/boot

	# Ajusta o boot para o novo pool
 	/sbin/zpool set bootfs=$pool/root 2> /dev/null
 	cat loader.conf | sed "s/POOL/$pool/g" > /mnt/boot/loader.conf

	tmp="/tmp/"`uuidgen`
	tmp2="/tmp/"`uuidgen`

	cp rc.conf $tmp

	# Aplica  HOSTNAME
	cat $tmp | sed "s/HOSTNAME/$hostname/g" > $tmp2
	cp  $tmp2 $tmp

        # Identificacao no arquivo hosts
	echo "$ip $hostname" >> /mnt/etc/hosts

	# Aplica nome da interface
	cat $tmp | sed "s/INTERFACE/$interface/g" > $tmp2
	cp  $tmp2 $tmp

	if [ $len -ne $IP_AUTOMATICO ]
	then
		# Aplica  IP
		cat $tmp | sed "s/IP/$ip/g" > $tmp2
		cp  $tmp2 $tmp

		# Aplica  MASCARA
		cat $tmp | sed "s/MASCARA/$mascara/g" > $tmp2
		cp  $tmp2 $tmp

		# Aplica  GATEWAY
		cat $tmp | sed "s/GATEWAY/$gateway/g" > $tmp2
		cp  $tmp2 $tmp
		cp  $tmp /mnt/etc/rc.conf

		# Aplica  DNS
		echo "nameserver $dns_primario"    > /mnt/etc/resolv.conf
		echo "nameserver $dns_secundario" >> /mnt/etc/resolv.conf
	else
		# Aplica  DHCP
		cat $tmp | sed "s/inet IP netmask MASCARA/DHCP/g" > $tmp2
		cp  $tmp2 $tmp

		# desabilita  GATEWAY
		cat $tmp | sed "s/defaultrouter/#defaultrouter/g" > $tmp2
		cp  $tmp2 $tmp
		cp  $tmp /mnt/etc/rc.conf
	fi

	# Remove mensagem de introducao
	echo -n > /mnt/etc/motd

	# Copia logotipo
	cp splash.bmp /mnt/boot

	# Haibilita acesso ssh
	echo "PermitRootLogin yes" >> /mnt/etc/ssh/sshd_config

	# Configura fstab
	echo "proc /proc procfs rw 0 0" >> /mnt/etc/fstab
	echo "fdesc /dev/fd fdescfs rw 0 0" >> /mnt/etc/fstab
	
	# Pos-instalacao
	cp pos-instalacao.sh /mnt/usr/bin

        # Entrar no mysql 
        cp mysql.sh /mnt/usr/bin

	tmp="/tmp/"`uuidgen | cut -d '-' -f 1`

	# Copia pacotes adicionais
	cp /usr/local/sbin/pkg-static /mnt/usr/bin
	
	#cp -r /cdrom/pkg /mnt
	./copydir.sh "Copiando pacotes" /cdrom/pkg /mnt

	cp instala_pacotes.sh /mnt/usr/bin
	
	# Habilita pos-instalacao
	cat /mnt/etc/rc | grep -v "exit 0"      >   $tmp
	echo "su root -c /usr/bin/pos-instalacao.sh"       >>  $tmp
	echo "exit 0" 			 	>>  $tmp
	cat $tmp > /mnt/etc/rc
	
	mkdir /mnt/conf
	
	cp diag_bsd.sh /mnt/conf
	cp httpd.conf  /mnt/conf
	cp index.html  /mnt/conf
	
	_concluido

	$REBOOT

}

_instalar $1

