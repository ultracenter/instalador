#!/bin/sh

echo "Content-type: text/html"
echo ""

tmp="/tmp/$RANDOM"

echo "<pre>"

d=`date | tr ' ' '_'`

printf "Bios date : %s\n" $d | tr '_' ' '
echo ""

uname -a 

echo ""
dmesg | grep memory | grep real 

echo ""
top -d 1 | grep '^Mem:' | cut -d ',' -f 4 | cut -c2-

/sbin/camcontrol devlist

echo ""

/sbin/gpart show

echo ""

/sbin/zpoool status
echo ""
/sbin/zpool list
echo ""
/sbin/zpool status

echo ""

camcontrol devlist | tr '\<' ' ' | tr '\>' ' ' 

echo ""

df -h 

echo ""

/sbin/ifconfig 

echo ""

/sbin/route -n

echo ""

#cat /var/dmidecode.log

#echo ""

set

