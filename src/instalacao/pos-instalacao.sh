#!/bin/sh

echo "Aguarde ..."

cd /pkg

# Pkg #################################################
pkg-static add pkg-1.12.0.txz

# Configura pkg #######################################
sed  -e 's/\#DEFAULT_ALWAYS_YES = false/DEFAULT_ALWAYS_YES = true/g' -e 's/\#ASSUME_ALWAYS_YES = false/ASSUME_ALWAYS_YES = true/g' /usr/local/etc/pkg.conf.sample > /tmp/pkg.conf
mv -f /tmp/pkg.conf /usr/local/etc/pkg.conf

# Instala os pacotes ##################################
/usr/bin/instala_pacotes.sh
#./instala_pacotes.sh "Instalando pacotes..."

# Editor ##############################################
mv /usr/bin/vi /usr/bin/vix
ln -s /usr/local/bin/mcedit /usr/bin/vi

# Banner ##############################################
/usr/local/bin/sysvbanner `/bin/hostname` > /etc/motd

# apache ##############################################
cp ../conf/httpd.conf /usr/local/etc/apache24
ln -s /usr/local/www/apache24/data /www
ln -s /usr/local/www/apache24/cgi-bin /
cp ../conf/diag_bsd.sh /cgi-bin
cp ../conf/index.html /www

#Desativa proxima chamada a este script ###############
cd /
cp $0 $0.completado
echo "clear" > $0

#Remove residuo #######################################
#cd /
#rm -rf /pkg
#rm -rf /conf

#Completado ##########################################
cd /
echo "Completado."
reboot

