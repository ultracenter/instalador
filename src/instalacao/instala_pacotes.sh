#!/bin/sh

clear
echo "Instalando pacotes..."

tmp="/tmp/instala_pacotes.$RANDOM"

echo -n > tmp

while true
do

    a_instalar=`ls *.txz | wc -l`
    a_instalar=`echo $a_instalar`
   
    #echo "a_instalar:$a_instalar"
 
    pkg-static add *.txz > $tmp

    instalados=`cat $tmp | grep installed | wc -l`
    instalados=`echo $instalados`

    #echo "instalados:$instalados"
    
    if [ $a_instalar == $instalados ]
    then
	clear
	echo "Pacotes instalados."
	exit
    fi

done


