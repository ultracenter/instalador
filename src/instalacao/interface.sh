#!/bin/bash

. ./titulo.sh

_interface()
{

	_ether()
	{
		local a=`/sbin/ifconfig $1 ether | tail -1 | cut -d ' ' -f 2`
		printf "[%s]" $a
	}


	_()
	{
	    local a
	    for i in `/sbin/ifconfig -a | grep -v lo | grep -v pfsync | grep -v ng | grep flags | grep mtu | cut -d ':' -f 1`;
	    do
		a=`_ether $i`
		echo $i "$a"
	    done
	}

	i=`_`

	dialog             \
	   --backtitle "$titulo" \
	   --title 'Interface'  \
	   --menu ' '  \
	   0 0 0             \
	   $i 2> /tmp/dialog

}

#_interface

