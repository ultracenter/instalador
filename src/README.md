

#  Gerador de instalador para FreeBSD

Este utilitario em shell script cria um instalador personalizado com bootsplash e pacotes selecionados.


# Componentes

* MfsBSD  : BSD compacto baseado em RAM utilizado para dar partida no instalador.
* FreeBSD : BSD que sera' instalado no dispositivo de destino.


* instalacao/ : Contem os arquivos de partida e o instalador que serao copiados para a midia de instalacao.

* freebsd/kernel.txz e freebsd/base.txz : Contem o Sistema Operacional FreeBSD a ser instalado. Esta' inclusa a versao 12.0-amd64, podendo-se trocar por sua versao preferida, bastando baixar em ftp.freebsd.org.

* kernel/  : Contem um kernel 12.0-amd64 recompilado com as opcoes VIMAGE, NETGRAPH, ALTQ  e IPFIREWALL ativadas. Instalacao opcaional. Padrao : instalado automaticamente.

* pkg/ : Contem os pacotes e dependencias que serao automaticamente instalados.

* iso/ : Contem uma imagem ISO de boot totalmente funcional com todas as pastas acima inclusas, fazendo com que ele possa se autoremasterizar.

* mfsbsd.tar.gz : Contem o mfsBSD

* autoboot/ : contem o gerador


# Procedimentos para gerar uma nova imagem ISO personalizada :

mkdir /cdrom
mount -t cd9660 /dev/cd0 /cdrom

mkdir /src
cd /src
tar -zxf /cdrom/mfsbsd.tar.gz
cd mfsbsd

make iso BASE=/cdrom/kernel , onde ficam os arquivos kernel.txz e base.txz

# Importante

* Quando o make exibir a mensagem : "compressing /usr..." , mantenha o teminal corrente aberto, abra outro terminal e digite cd /cdrom/autoboot &&  ./instalar.sh

* Os processos de criacao e customizacao da imagem ISO irao ocorrer em paralelo.

* Aguarde a finalizacao dos processos.





